This repository contains riscv-linux with modifications for shakti processors


Usage:
===
Follow the below steps to build the linux

* Make sure there is no `riscv*` tools already available in your `$PATH`

* Follow these steps to build the kernel 
    
            $ git clone https://bitbucket.org/casl/shakti-linux.git
            $ cd shakti-linux
            $ git submodule update --init --recursive
            $ make -j16 ISA=rv64imafd

* There are two Configurations, one without networking and the other with networking enabled (named as linux_defconfig_networking under conf directory). Select the appropriate configuration by changing the linux_defconfig line in the Makefile. If you want your own customized Configuration, you can make changes to the configuration by using

            $ make menuconfig

     inside the riscv-linux subdirectory

